<?php

namespace Wizeoo\ContactBundle\Controller;

use Wizeoo\ContactBundle\Entity\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

// /**
//  * Contact controller.
//  *
//  * @Route("contact")
//  */
class ContactController extends Controller
{

    public function homeAction()
    {
        $em = $this->getDoctrine()->getManager();

        $contacts = $em->getRepository('WizeooContactBundle:Contact')->findAll();

        return $this->render('WizeooContactBundle:Contact:home.html.twig', array(
            'contacts' => $contacts,
        ));
    }


    public function newAction(Request $request)
    {
        $contact = new Contact();
        $form = $this->createForm('Wizeoo\ContactBundle\Form\ContactType', $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();
            // return $this->redirectToRoute('contact_show', array('id' => $contact->getId()));
        }

        return $this->render('contact/new.html.twig', array(
            'contact' => $contact,
            'form' => $form->createView(),
        ));
    }


    public function showAction(Contact $contact)
    {
        $deleteForm = $this->createDeleteForm($contact);
        return $this->render('contact/show.html.twig', array(
            'contact' => $contact,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    // /**
    //  * Displays a form to edit an existing contact entity.
    //  *
    //  * @Route("/{id}/edit", name="contact_edit")
    //  * @Method({"GET", "POST"})
    //  */
    public function editAction(Request $request, Contact $contact)
    {
        $deleteForm = $this->createDeleteForm($contact);
        $editForm = $this->createForm('Wizeoo\ContactBundle\Form\ContactType', $contact);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('wizeoo_edit_contact', array('id' => $contact->getId()));
        }

        return $this->render('contact/edit.html.twig', array(
            'contact' => $contact,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    // /**
    //  * Deletes a contact entity.
    //  *
    //  * @Route("/{id}", name="contact_delete")
    //  * @Method("DELETE")
    //  */
    public function deleteAction(Request $request, Contact $contact)
    {
        $form = $this->createDeleteForm($contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($contact);
            $em->flush();
        }

        return $this->redirectToRoute('wizeoo_contact_homepage');
    }

    // /**
    //  * Creates a form to delete a contact entity.
    //  *
    //  * @param Contact $contact The contact entity
    //  *
    //  * @return \Symfony\Component\Form\Form The form
    //  */
    private function createDeleteForm(Contact $contact)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('wizeoo_delete_contact', array('id' => $contact->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
